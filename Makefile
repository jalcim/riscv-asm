hello: hello.o link.lds
	riscv64-unknown-elf-ld -T link.lds -o bin/hello tmp/hello.o

	riscv64-unknown-elf-ld -T link.lds -o bin/melimelo tmp/melimelo.o
	riscv64-unknown-elf-objcopy -O binary -j .text tmp/melimelo.o bin/melimelo.raw

	riscv64-unknown-elf-ld -T link.lds -o bin/instr tmp/instr.o
	riscv64-unknown-elf-objcopy -O binary -j .text tmp/instr.o bin/instr.raw

	riscv64-unknown-elf-ld -T link.lds -o bin/reg tmp/reg.o
	riscv64-unknown-elf-objcopy -O binary -j .text tmp/reg.o bin/reg.raw

hello.o:
	mkdir bin tmp
	riscv64-unknown-elf-as -o tmp/hello.o asm_src/hello.s
	riscv64-unknown-elf-as -o tmp/melimelo.o asm_src/melimelo.s
	riscv64-unknown-elf-as -o tmp/instr.o asm_src/instr.s
	riscv64-unknown-elf-as -o tmp/reg.o asm_src/reg.s

desassembleur:
	gcc -Wall -Werror C_src/desassembleur.c \
	C_src/opcode.c C_src/instr/op_imm.c C_src/instr/lui.c \
	C_src/instr/auipc.c C_src/instr/op.c C_src/instr/jal.c \
	C_src/instr/jalr.c C_src/instr/branch.c C_src/instr/load.c \
	C_src/instr/store.c C_src/imm/I_imm.c \
	C_src/imm/S_imm.c C_src/imm/B_imm.c \
	C_src/imm/U_imm.c C_src/imm/J_imm.c \
	C_src/utils.c C_src/method/method_op_imm.c \
	-o bin/desassembleur

C_test:
	riscv64-unknown-elf-gcc -Ttext -0x00 -Tbss 0x100020 -O1 -s -march=rv32i -mabi=ilp32 -c asm_src/test.c -o tmp/c_test.o
	riscv64-unknown-elf-objcopy -O binary -j .text -j .bss -S tmp/c_test.o bin/c_test.raw

clean:
	rm -rf tmp

clean_bin: clean
	rm -rf bin

all: hello desassembleur

re: clean_bin all
