#include "../header.h"
#include "../method/op_imm.h"

void OP_IMM(unsigned int *bin)
{
  I_type *instr = (I_type *)bin;
  t_op_imm op_imm =
    {
      .rd = instr->rd,
      .rs1 = instr->rs1,
      .imm = I_immediate(instr)
    };

  switch (instr->funct3)
    {
    case 0:
      method_addi(&op_imm);
      printf("addi ");
      break;
    case 1:
      method_slli(&op_imm);
      printf("slli ");
      break;
    case 2:
      method_slti(&op_imm);
      printf("slti ");
      break;
    case 3:
      method_sltiu(&op_imm);
      printf("sltiu ");
      break;
    case 4:
      method_xori(&op_imm);
      printf("xori ");
      break;
    case 5:
      printf("method not implemented for \n");
      printf("srai | srli ");
      break;
    case 6:
      method_ori(&op_imm);
      printf("ori ");
      break;
    case 7:
      method_andi(&op_imm);
      printf("andi ");
      break;
    default:
      break;
    }
  /*
  op_imm.imm |=
    (op_imm.imm & 0x80000000) ?
    0x400 : 0;
  op_imm.imm = op_imm.imm & 0x3ff;
  */
  printf("x%d, x%d 0x%x\n", instr->rd, instr->rs1, op_imm.imm);
}
