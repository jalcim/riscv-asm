#include "header.h"

int main(int argc, char **argv)
{
  t_binary *binary;

  if (argc < 2)
    {
      printf("you need to specify a binary file.\n");
      return (-1);
    }

  binary = binary_reader(argv[1]);
  OPCODE(binary->binary_raw, binary->sizefd);

  munmap(binary->binary_raw, binary->sizefd);
  close(binary->fd);
}
