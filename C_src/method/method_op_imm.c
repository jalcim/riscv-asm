#include <stdio.h>
#include "op_imm.h"

int method_addi(t_op_imm *op_imm)
{
  printf("addi rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_slti(t_op_imm *op_imm)
{
  printf("slti rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);  
}

int method_sltiu(t_op_imm *op_imm)
{
  printf("sltiu rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_andi(t_op_imm *op_imm)
{
  printf("andi rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_ori(t_op_imm *op_imm)
{
  printf("ori rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_xori(t_op_imm *op_imm)
{
  printf("xori rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_slli(t_op_imm *op_imm)
{
  printf("slli rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_srai(t_op_imm *op_imm)
{
  printf("srai rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_srli(t_op_imm *op_imm)
{
  printf("srli rd=0x%x, rs1=0x%x, imm=0x%x\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}
