typedef struct s_op_imm t_op_imm;
struct s_op_imm
{
  unsigned int rd;
  unsigned int rs1;
  unsigned int imm;
};

int method_addi(t_op_imm *op_imm);
int method_slti(t_op_imm *op_imm);
int method_sltiu(t_op_imm *op_imm);
int method_andi(t_op_imm *op_imm);
int method_ori(t_op_imm *op_imm);
int method_xori(t_op_imm *op_imm);
int method_slli(t_op_imm *op_imm);
int method_srai(t_op_imm *op_imm);
int method_srli(t_op_imm *op_imm);
