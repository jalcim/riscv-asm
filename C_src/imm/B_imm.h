#ifndef __B_IMM__
#define __B_IMM__

typedef struct s_B_imm B_imm;

struct s_B_imm
{
  unsigned int padding   : 1;
  unsigned int inst11_8  : 4;
  unsigned int inst30_25 : 6;
  unsigned int inst7     : 1;
  unsigned int inst31    : 20;  
}__attribute__((packed));

#endif
