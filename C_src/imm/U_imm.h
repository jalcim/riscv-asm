#ifndef __U_IMM__
#define __U_IMM__

typedef struct s_U_imm U_imm;

struct s_U_imm
{
  unsigned int padding   : 12;
  unsigned int inst19_12 : 8;
  unsigned int inst30_20 : 11;  
  unsigned int inst31    : 1;  
}__attribute__((packed));

#endif
