#ifndef __J_IMM__
#define __J_IMM__

typedef struct s_J_imm J_imm;

struct s_J_imm
{
  unsigned int padding   : 1;
  unsigned int inst24_21 : 4;
  unsigned int inst30_25 : 6;
  unsigned int inst20    : 1;
  unsigned int inst19_12 : 8;
  unsigned int inst31    : 12;
}__attribute__((packed));

#endif
