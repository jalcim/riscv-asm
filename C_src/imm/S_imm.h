#ifndef __S_IMM__
#define __S_IMM__

typedef struct s_S_imm S_imm;

struct s_S_imm
{
  unsigned int inst7     : 1;
  unsigned int inst11_8  : 4;
  unsigned int inst30_25 : 6;
  unsigned int inst31    : 21;
}__attribute__((packed));

#endif
