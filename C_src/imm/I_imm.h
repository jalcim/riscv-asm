#ifndef __I_IMM__
#define __I_IMM__

typedef struct s_I_imm I_imm;

struct s_I_imm
{
  unsigned int inst31     : 21;
  unsigned int inst30_25  : 6;
  unsigned int inst24_21  : 4;
  unsigned int inst20     : 1;
}__attribute__((packed));

#endif
