#ifndef __R_TYPE__
#define __R_TYPE__

typedef struct s_R_type R_type;
struct s_R_type
{
  unsigned int opcode : 7;
  unsigned int rd     : 5;
  unsigned int funct3 : 3;
  unsigned int rs1    : 5;
  unsigned int rs2    : 5;
  unsigned int funct7 : 7;
}__attribute__((packed));

#endif
