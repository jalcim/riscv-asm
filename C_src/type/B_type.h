#ifndef __B_TYPE__
#define __B_TYPE__

typedef struct s_B_type B_type;
struct s_B_type
{
  unsigned int opcode : 7;
  unsigned int imm11  : 1;
  unsigned int imm4_1 : 4;
  unsigned int funct3 : 3;
  unsigned int rs1    : 5;
  unsigned int rs2    : 5;
  unsigned int imm10_5: 6;
  unsigned int imm12  : 1;
}__attribute__((packed));

#endif
