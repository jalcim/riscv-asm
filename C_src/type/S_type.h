#ifndef __S_TYPE__
#define __S_TYPE__

typedef struct s_S_type S_type;
struct s_S_type
{
  unsigned int opcode : 7;
  unsigned int imm4_0 : 5;
  unsigned int funct3 : 3;
  unsigned int rs1    : 5;
  unsigned int rs2    : 5;
  unsigned int imm11_5: 7;
}__attribute__((packed));

#endif
