#ifndef __U_TYPE__
#define __U_TYPE__

typedef struct s_U_type U_type;
struct s_U_type
{
  unsigned int opcode   : 7;
  unsigned int rd       : 5;
  unsigned int imm31_12 : 20;
}__attribute__((packed));

#endif
