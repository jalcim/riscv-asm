#ifndef __I_TYPE__
#define __I_TYPE__

typedef struct s_I_type I_type;
struct s_I_type
{
  unsigned int opcode : 7;
  unsigned int rd     : 5;
  unsigned int funct3 : 3;
  unsigned int rs1    : 5;
  unsigned int imm11_0: 12;
}__attribute__((packed));

#endif
