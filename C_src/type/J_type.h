#ifndef __J_TYPE__
#define __J_TYPE__

typedef struct s_J_type J_type;
struct s_J_type
{
  unsigned int opcode   : 7;
  unsigned int rd       : 5;
  unsigned int imm19_12 : 8;
  unsigned int imm11    : 1;
  unsigned int imm10_1  : 10;
  unsigned int imm20    : 1;
}__attribute__((packed));

#endif
