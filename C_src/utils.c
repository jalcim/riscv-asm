#include "header.h"
#include <stdlib.h>

t_binary *binary_reader(char *argv)
{
  t_binary *binary = (t_binary *) malloc(sizeof(t_binary));

  char filename[256];
   
  strncpy(filename, argv, 255);
  filename[255] = 0;
  binary->fd = open(filename, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
  if (binary->fd == -1)
    {
      perror(filename);
      return (NULL);
    }

  binary->sizefd = size_fd(binary->fd);
  binary->binary_raw = ft_readfd(binary->fd, binary->sizefd);
  return (binary);
}

void print_bin(unsigned int instr, int size)
{
  unsigned int mask = 1 << (size -1);
  int cpt;

  cpt = -1;
  while (++cpt < size)
    {
      printf("%d", mask & instr ? 1 : 0);
      mask >>= 1;
    }
}

char *ft_readfd(int fd, int nb_oct)
{
  char *str;

  str = (char *)projectm(fd, PROT_READ, nb_oct);

  return (str);
}

char *ft_writefd(int fd, int nb_oct)
{
  char *str;

  str = (char *)projectm(fd, PROT_WRITE, nb_oct);

  return (str);
}

int size_fd(int fd)
{
  struct stat status;

  fstat(fd, &status);

  return (status.st_size);
}

void *projectm(int fd, int prot, int nb_oct)
{
  void *addr = NULL;

  addr = mmap(addr, nb_oct, prot, MAP_SHARED, fd, 0);
  if (addr == MAP_FAILED)
    perror("mmap -> ");
  msync(addr, nb_oct, 0);
  return (addr);
}
