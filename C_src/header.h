#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>

#include "imm/B_imm.h"
#include "imm/I_imm.h"
#include "imm/S_imm.h"
#include "imm/J_imm.h"
#include "imm/U_imm.h"

#include "type/R_type.h"
#include "type/I_type.h"
#include "type/S_type.h"
#include "type/B_type.h"
#include "type/J_type.h"
#include "type/U_type.h"

//------------------------------

typedef struct s_binary t_binary;
struct s_binary
{
  char *binary_raw;
  int sizefd;
  int fd;
};

void OPCODE(char *binary_raw, int sizefd);

void OP_IMM(unsigned int *instr);
void LUI(unsigned int *instr);
void AUIPC(unsigned int *instr);
void OP(unsigned int *instr);
void JAL(unsigned int *instr);
void JALR(unsigned int *instr);
void BRANCH(unsigned int *instr);
void LOAD(unsigned int *instr);
void STORE(unsigned int *instr);

unsigned int I_immediate(I_type *instr);
unsigned int S_immediate(S_type *instr);
unsigned int B_immediate(B_type *instr);
unsigned int U_immediate(U_type *instr);
unsigned int J_immediate(J_type *instr);

void print_bin(unsigned int instr, int size);
void *projectm(int fd, int prot, int nb_oct);
int size_fd(int fd);
char *ft_writefd(int fd, int nb_oct);
char *ft_readfd(int fd, int nb_oct);

t_binary *binary_reader(char *argv);
