#include "../simulator.h"
#include <stdio.h>

int method_addi(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
  printf("op_imm addi rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_slti(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
  printf("op_imm slti rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);  
}

int method_sltiu(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
  printf("op_imm sltiu rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_andi(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
  printf("op_imm andi rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_ori(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
  printf("op_imm ori rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_xori(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
  printf("op_imm xori rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_slli(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
    printf("op_imm slli rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_srai(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
  printf("op_imm srai rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}

int method_srli(void *arg)
{
  t_op_imm *op_imm = (t_op_imm *)arg;
  printf("op_imm srli rd=%d, rs1=%d, imm=%d\n",
	 op_imm->rd, op_imm->rs1, op_imm->imm);
  return (0);
}
