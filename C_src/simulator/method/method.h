int method_addi(void *arg);
int method_slti(void *arg);
int method_sltiu(void *arg);
int method_andi(void *arg);
int method_ori(void *arg);
int method_xori(void *arg);
int method_slli(void *arg);
int method_srai(void *arg);
int method_srli(void *arg);
