typedef struct s_method_opcode t_method_opcode;
typedef struct s_op_imm t_op_imm;
typedef struct s_op t_op;

struct s_op_imm
{
  int rd;
  int rs1;
  int imm;
};

struct s_op
{
  int rd;
  int rs1;
  int rs2;
};

typedef struct s_register t_register;
struct s_register
{
  int reg[32];
};
