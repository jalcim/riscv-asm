#include "header.h"
#include "simulator.h"
#include "method/method.h"

void method_initializer(int (*method_tab[11][20])(void *arg));
void method_initializer_op_imm(int (*method_tab[20])(void *arg));

void method_initializer(int (*method_tab[11][20])(void *arg))
{
  method_initializer_op_imm(method_tab[0]);
  method_initializer_op    (method_tab[1]);

}

void method_initializer_op_imm(int (*method_tab[20])(void *arg))
{
  //  method_tab = (int (*[20])(void *)) {method_addi, method_slti,
  //    method_sltiu, method_andi, method_ori, method_xori,
  //    method_slli, method_srai, method_srli};

  method_tab[0] = method_addi;
  method_tab[1] = method_slti;
  method_tab[2] = method_sltiu;
  method_tab[3] = method_andi;
  method_tab[4] = method_ori;
  method_tab[5] = method_xori;
  method_tab[6] = method_slli;
  method_tab[7] = method_srai;
  method_tab[8] = method_srli;
}

void method_initializer_op_imm(int (*method_tab[20])(void *arg))
{
  method_tab[0] = ;
  method_tab[0] = ;
  method_tab[0] = ;
  method_tab[0] = ;
  method_tab[0] = ;
  method_tab[0] = ;
  method_tab[0] = ;
  method_tab[0] = ;
  method_tab[0] = ;
  method_tab[0] = ;
}

int main(int argc, char **argv)
{
  int (*method_tab[11][20]) (void *arg);

  method_initializer(method_tab);

  t_op_imm op_imm = (t_op_imm){0, 2, 4};
  (*method_tab[0][0])(&op_imm);
}
/*
  t_binary *binary;

    if (argc < 2)
    {
      printf("you need to specify an binnary file.\n");
      return (-1);
    }

    binary = binary_reader(argv[1]);
    
}
*/
